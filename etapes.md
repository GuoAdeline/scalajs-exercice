## Step 0: Prerequisites
#### 1. download and install `sbt` 
- Install JDK (Java SE Development Kit 8)
- Windows installer: download msi installer and install it

#### 2. download and install `Node.js`

## Step 1: Setup
#### 1. sbt Setup
<span style="color:blue"> - Setup a simple Hello world build: type `sbt new sbt/scala-seed.g8` <br/> - Running app: Now from inside the hello directory, start sbt and type run at the sbt shell. `cd hello` -> `sbt` -> `run`  <br/>- Exiting sbt shell: type `exit` ou `Ctrl+C`</span>
- To setup Scala.js in a new sbt project, we need to do two things: 
   * Add the Scala.js sbt plugin to the build. 
   * Enable the plugin in the project
- Adding the Scala.js sbt plugin is a one-liner in ** project/plugins.sbt ** : `addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.26")`
- setup basic project settings and enable this plugin in the sbt build file (**build.sbt**, in the project root directory):

 ```
 enablePlugins(ScalaJSPlugin)
 
 name := "Scala.js Tutorial"

 scalaVersion := "2.12.6" // or any other Scala version >= 2.10.2

 // This is an application with a main method
 
 scalaJSUseMainModuleInitializer := true
 ```
 
- Last, we need a **project/build.properties** to specify the sbt version (>= 1.2.7):

   `sbt.version=1.2.7`
 
 ## For Linux / Mac user
- Use image **httpd**. Copy and paste to pull this image `docker pull httpd`
- In the Dockerfile: 
```
FROM httpd:2.4
COPY ./target/scala-2.12/scala-js-exercice-fastopt.js /usr/local/apache2/htdocs/
COPY ./scalajs-exercice.html /usr/local/apache2/htdocs/
```

Then, run the commands to build and run the Docker image:
```
docker build -t my-apache2 .
$ docker run -dit --name myapache -p 8080:80 my-apache2
```
Visit http://localhost:8080/hello.html and you will see `Hello World`
