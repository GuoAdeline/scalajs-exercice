## Prerequisites
#### 1. download and install `sbt` 
- Install JDK (Java SE Development Kit 8)
- Windows installer: download msi installer and install it

#### 2. download and install `Node.js`

 ## Visualiser la page web
- Use image **httpd**. Copy and paste to pull this image `docker pull httpd`
- In the Dockerfile: 
```
FROM httpd:2.4
COPY ./target/scala-2.12/scala-js-exercice-fastopt.js /usr/local/apache2/htdocs/
COPY ./scalajs-exercice.html /usr/local/apache2/htdocs/
```

- Then, run the commands to build and run the Docker image:
```
docker build -t my-apache2 .
docker run -dit --name myapache -p 8080:80 my-apache2
```
Visit http://localhost:8080/hello.html and you will see `Hello World`

## changer la phrase "Hello World" par "Hello OpenMOLE"

- Edit in: `.\src\main\scala\example\Hello.scala`,
change "Hello World" to "Hello OpenMOLE"
```console
hello> sbt
sbt:Scala.js Exercice> fastOptJS
sbt:Scala.js Exercice> exit
```
```console
hello> docker stop myapache
hello> docker container rm myapache
hello> docker build -t my-apache2 .
hello> docker run -dit --name myapache -p 8080:80 my-apache2
```
Visit http://localhost:8080/hello.html and you will see `Hello OpenMOLE`